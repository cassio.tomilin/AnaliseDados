﻿using AnaliseDados.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace AnaliseDados.Program
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                // Busca automaticamente todos os arquivos .dat em todos os subdiretórios dentro da pasta in
                var dir = new DirectoryInfo(Path.GetDirectoryName(Path.GetDirectoryName(Directory.GetCurrentDirectory())) + "\\data\\in");

                var arquivos = dir.GetFiles("*dat", SearchOption.AllDirectories);

                //Criando as listas
                var listaClientes = new List<Clientes>();
                var listaVendedores = new List<Vendedores>();
                var listaVendaItens = new List<VendaItens>();

                foreach (var arquivo in arquivos)
                {
                    var file = File.ReadAllText(arquivo.FullName);
                    var lines = file.Split(new string[] { "\r\n" }, StringSplitOptions.RemoveEmptyEntries);

                    if (lines.Length > 0)
                    {
                        var vendedores = lines.Where(x => x.Substring(0, 3) == "001");
                        PopulaVendedores(vendedores, listaVendedores);

                        var clientes = lines.Where(x => x.Substring(0, 3) == "002");
                        PopulaClientes(clientes, listaClientes);

                        var vendasrealizadas = lines.Where(x => x.Substring(0, 3) == "003");
                        PopulaVendaItens(vendasrealizadas, listaVendaItens);
                    }
                }

                CriarArquivoResultado(listaClientes, listaVendedores, listaVendaItens);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        private static void CriarArquivoResultado(List<Clientes> listaclientes, List<Vendedores> listaVendedores, List<VendaItens> listaVendaItens)
        {
            var fileOutput = File.CreateText(Path.GetDirectoryName(Path.GetDirectoryName(Directory.GetCurrentDirectory())) + "\\data\\out\\resultado_" + DateTime.Now.ToString().Replace(" ", "_").Replace("/", "_").Replace(":", "_") + ".done.dat");

            fileOutput.WriteLine("Quantidade de clientes nos arquivos de entrada: {0} ", listaclientes.Count());

            fileOutput.WriteLine("Quantidade de vendedores nos arquivos de entrada: {0} ", listaVendedores.Count());

            var maiorVenda = listaVendaItens.GroupBy(c => c.SaleID)
                .Select(s => new
                {
                    Id = s.Key,
                    Venda = s.Sum(v => v.Total),
                }).OrderByDescending(a => a.Venda).ToList();

            fileOutput.WriteLine("ID da venda mais cara: {0}", maiorVenda.FirstOrDefault().Id);

            var piorVendedor = listaVendaItens.GroupBy(c => c.SalesmanName)
                .Select(s => new
                {
                    Id = s.Key,
                    Venda = s.Sum(v => v.Total),
                }).OrderBy(a => a.Venda).ToList();

            fileOutput.WriteLine("O pior vendedor: {0}", piorVendedor.FirstOrDefault().Id);

            fileOutput.Close();
        }

        private static void PopulaVendaItens(IEnumerable<string> vendasrealizadas, List<VendaItens> listaVendaItens)
        {
            var vendasArrayString = new string[] { };
            var itensVendaArrayString = new string[] { };
            var itemArrayString = new string[] { };

            foreach (var item in vendasrealizadas)
            {
                //conversão de string to array
                var chars = item.Replace("[", "").Replace("]", "");
                vendasArrayString = chars.Split(new char[] { '�' });
                itensVendaArrayString = vendasArrayString[2].Split(new char[] { ',' });
                foreach (var itensvenda in itensVendaArrayString)
                {
                    itemArrayString = itensvenda.Split(new char[] { '-' });

                    //convertido para double para realizar o calculo
                    listaVendaItens.Add(new VendaItens
                    {
                        ItemID = double.Parse(itemArrayString[0], System.Globalization.CultureInfo.InvariantCulture),
                        ItemQuantity = double.Parse(itemArrayString[1], System.Globalization.CultureInfo.InvariantCulture),
                        ItemPrice = double.Parse(itemArrayString[2], System.Globalization.CultureInfo.InvariantCulture),
                        SaleID = vendasArrayString[1],
                        SalesmanName = vendasArrayString[3]
                    });
                }
            }
        }

        private static void PopulaClientes(IEnumerable<string> clientes, List<Clientes> listaClientes)
        {
            foreach (var item in clientes)
            {
                var split = item.Split(new char[] { '�' });

                listaClientes.Add(new Clientes
                {
                    CNPJ = split[1],
                    Name = split[2],
                    BusinessArea = split[3]
                });
            }
        }

        private static void PopulaVendedores(IEnumerable<string> vendedores, List<Vendedores> listaVendedores)
        {
            foreach (var item in vendedores)
            {
                var split = item.Split(new char[] { '�' });

                listaVendedores.Add(new Vendedores
                {
                    CPF = split[1],
                    Name = split[2],
                    Salary = split[3]
                });
            }
        }
    }
}
