# REQUISITOS

Você deve criar um sistema de análise de dados, onde poderá importar lotes de arquivos, ler e analisar os dados e produzir um relatório. Existem 3 tipos de dados dentro desses arquivos.
Para cada tipo de dado há um layout diferente.

Dados do vendedor:

	Os dados do vendedor têm o formato id 001 e a linha terá o seguinte formato.
	001çCPFçNameçSalary

Dados do cliente:

	Os dados do cliente têm o formato id 002 e a linha terá o seguinte formato.
	002çCNPJçNameçBusiness Area

Dados de vendas:

	Os dados de vendas têm o formato id 003. Dentro da linha de vendas, existe a lista de itens, que é envolto por colchetes []. 
	A linha terá o seguinte formato.
	
	003çSale IDç[Item ID-Item Quantity-Item Price]çSalesman name

Dados de Exemplo:

Segue abaixo um exemplo dos dados que o sistema deve ser capaz de ler.

	001ç1234567891234çDiegoç50000 
	001ç3245678865434çRenatoç40000.99 
	002ç2345675434544345çJose da SilvaçRural 
	002ç2345675433444345çEduardo PereiraçRural 
	003ç10ç[1-10-100,2-30-2.50,3-40-3.10]çDiego 
	003ç08ç[1-34-10,2-33-1.50,3-40-0.10]çRenato
	
Análise de dados:

Seu sistema deve ler os dados do diretório padrão, localizado em %HOMEPATH% /data/in. O sistema só deve ler arquivos .dat
Depois de processar todos os arquivos dentro do diretório padrão de entrada, o sistema deve criar um arquivo dentro do diretório de saída padrão, localizado em %HOMEPATH% /data/out.
O nome do arquivo deve seguir o padrão {flat_file_name} .done .dat.

O conteúdo do arquivo de saída deve resumir os seguintes dados:

Quantidade de clientes no arquivo de entrada
Quantidade de vendedores no arquivo de entrada
ID da venda mais cara
O pior vendedor
 


# COMO EXECUTAR

O projeto é um Console Application... então é necessário só rodar o app que irá realizar as funções solicitadas.
Deixei 3 arquivos .dat na pasta in... e um arquivo .txt.(O sistema irá ler somente os arquivos .dat).
Após a execução do projeto irá salvar um arquivo na pasta data/out, conforme solicitado.

# ESTRUTURA DO PROJETO

Domain:
	Camada de domínio, contendo os modelos (Entities).
Program:
	Run do Console Application padrão...optei por fazer o mais simples possível.